﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIODemo
{
    public static class PrintToCsv //New class created - for printing to CSV - access the old class (ItemList.cs)
    //Create an extension method in ths new class
    {
        public static void WriteToCSVFileTwo(this ItemList o) //declare extension method
        {
            try
            {
                using StreamWriter sw = new StreamWriter("@itemtwo.csv");
                //Csv writer need a default settings - culstrueinfor.invariantculture
                using CsvWriter writer = new CsvWriter(sw, CultureInfo.InvariantCulture);
                //Add items from the list - by invoking GetItems method
                writer.WriteRecords<Item>(ItemList.GetItemName());                 
                    
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }


        }



    }
}
