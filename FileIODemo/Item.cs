﻿namespace FileIODemo
{
    public class Item
    {
        //Set up the properties for the newly created list.
        public string Name { get; set; }
        public int ManufactingYear { get; set; }
        public double Price { get; set; }

        //Default constructor
        public Item()
        {
        }



        //Constructor for the Name property
        public Item(string name)
        {
            Name = name;
        }

        

    }
}