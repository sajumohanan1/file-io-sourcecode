﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace FileIODemo
{
    class Program
    {
        static void Main(string[] args)
        {
            //Invode the class for the extension method

            ItemList filetwo = new ItemList();
            filetwo.WriteToCSVFileTwo();

            //Invoke the method from the main program

            //Write to the text file
            //WriteToTextFile();

            //Read from the text file
            //ReadFromTextFile();

            //Write to the csv file
            //WriteToCSVFile();

            //Read from the csv file
            // ReadFromCSVFile();

            Console.ReadKey();

            //Extension methods = Inheritance

        }

        static List<Item> GetItems()
        {
            List<Item> items = new List<Item>()
            {
                new Item() { Name="Calculator",ManufactingYear=2022, Price=550.100 },
                new Item() { Name="Note Book",ManufactingYear=2021, Price=8.70 },
                new Item() { Name="Printer",ManufactingYear=2020, Price=300.50 }
            };
            return items;
        } 
        
   
        

        //Create a list for the list of items
        
        static void WriteToTextFile()
        {
            List<Item> items = GetItems(); //Invoked GetItems method
            try
            {               

                //Write these item lists into a file (txt)
                //Class - StreamWriter
                //using keyword calls the Dispose method
                //SQL, database - using
                //An items.txt file created for the sr object.

                //Keyword to append text into a file

                using StreamWriter sr = new StreamWriter("@items.txt", true);
                sr.WriteLine(items[0].Name + " " + items[0].ManufactingYear + " " + items[0].Price);
                sr.WriteLine(items[1].Name + " " + items[1].ManufactingYear + " " + items[1].Price);
                sr.WriteLine(items[2].Name + " " + items[2].ManufactingYear + " " + items[2].Price);
                //sr.Close();
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }         
            

        }
        static void ReadFromTextFile()
        {
            try
            {
                using StreamReader sr = new StreamReader("@items.txt");

                //To read line by line - logic
                string Line = string.Empty;

                //read line by line using a loop
                //In while loop, you are calling the Readline on sr object.
                //Assign the value to the Line variable
                while((Line= sr.ReadLine())!=null) //print item if not null
                {
                    Console.WriteLine(Line); //Writing to the console window.
                }

        }
            catch (FileNotFoundException ex)
            {

                Console.WriteLine(ex.Message);
            }
}

        static void WriteToCSVFile()
        {
            try
            {
                using StreamWriter sw = new StreamWriter("@items.csv");
                //Csv writer need a default settings - culstrueinfor.invariantculture
                using CsvWriter writer = new CsvWriter(sw, CultureInfo.InvariantCulture);
                //Add items from the list - by invoking GetItems method
                writer.WriteRecords<Item>(GetItems());
            }
            catch (Exception ex)
            {

                Console.WriteLine(ex.Message);
            }
            
            
        }

        static void ReadFromCSVFile()
        {
            try
            {
                //Assign the file name to a variable and call the variable to execute the file
                //const string Text_File1 = @"items.csv";



                using StreamReader sr = new StreamReader("@items.csv");
                using CsvReader reader = new CsvReader(sr, CultureInfo.InvariantCulture);

                //Reading records from the CSV file and print it int he console
                //IEnumberable
                IEnumerable<Item> itemsFromCsvFile = reader.GetRecords<Item>();

                foreach (var item in itemsFromCsvFile)
                {
                    Console.WriteLine($"{item.Name},  {item.ManufactingYear},  {item.Price}");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }            
        }
    }  
    
}
