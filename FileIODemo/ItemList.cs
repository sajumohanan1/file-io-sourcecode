﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIODemo
{
    public class ItemList //As the main class/exisiting class/old class
    {
        public static List<Item> GetItemName()
        {
            List<Item> items = new List<Item>()
            {
                new Item("Item No 1"),
                new Item("Item No 2"),
                new Item("Item No 3")
            };
            return items;
        }
        //Not able to modify....
    }
}
